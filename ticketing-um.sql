-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.19 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table db_ticket.applications
CREATE TABLE IF NOT EXISTS `applications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.applications: ~4 rows (approximately)
/*!40000 ALTER TABLE `applications` DISABLE KEYS */;
INSERT INTO `applications` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'EPROC', '2018-05-30 18:15:02', '2018-05-30 18:15:02'),
	(2, 'Sistem Booking', '2018-05-30 18:15:02', '2018-05-30 18:15:02'),
	(3, 'E-kemajuan', '2018-05-30 18:15:02', '2018-05-30 18:15:02'),
	(4, 'E-KESELAMATAN', '2018-05-30 18:15:02', '2018-05-30 18:15:02');
/*!40000 ALTER TABLE `applications` ENABLE KEYS */;

-- Dumping structure for table db_ticket.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.departments: ~6 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`id`, `name`, `email`, `created_at`, `updated_at`) VALUES
	(1, 'Bahagian Akaun Dan Pelaburan', 'akaun@um.com', '2018-05-30 18:15:24', '2018-05-30 18:15:24'),
	(2, 'Bahagian Pembayaran Dan Perbelanjaan', 'bayar@um.com', '2018-05-30 18:15:24', '2018-05-30 18:15:24'),
	(3, 'Bahagian Pengurusan Pembayaran Kewangan Dan Staf', 'urusakaun@um.com', '2018-05-30 18:15:24', '2018-05-30 18:15:24'),
	(4, 'Bahagian Belanjawan Dan Kewangan Korporat', 'belanjawan@um.com', '2018-05-30 18:15:24', '2018-05-30 18:15:24'),
	(5, 'Bahagian Perolehan', 'oleh@um.com', '2018-05-30 18:15:24', '2018-05-30 18:15:24'),
	(6, 'Unit Pentadbiran Dan Kewangan Am', 'tadbir@um.com', '2018-05-30 18:15:24', '2018-05-30 18:15:24');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table db_ticket.department_user
CREATE TABLE IF NOT EXISTS `department_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `department_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `department_user_user_id_foreign` (`user_id`),
  KEY `department_user_department_id_foreign` (`department_id`),
  CONSTRAINT `department_user_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `department_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.department_user: ~2 rows (approximately)
/*!40000 ALTER TABLE `department_user` DISABLE KEYS */;
INSERT INTO `department_user` (`id`, `user_id`, `department_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 4, NULL, NULL),
	(2, 4, 4, NULL, NULL);
/*!40000 ALTER TABLE `department_user` ENABLE KEYS */;

-- Dumping structure for table db_ticket.linked_social_accounts
CREATE TABLE IF NOT EXISTS `linked_social_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `linked_social_accounts_provider_id_unique` (`provider_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.linked_social_accounts: ~0 rows (approximately)
/*!40000 ALTER TABLE `linked_social_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_social_accounts` ENABLE KEYS */;

-- Dumping structure for table db_ticket.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.migrations: ~54 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_04_12_142435_prepare_users_table_for_social_authentication', 1),
	(4, '2018_04_12_142527_create_linked_social_accounts_table', 1),
	(5, '2018_04_16_130049_create_permission_tables', 1),
	(6, '2018_04_17_083355_create_saps_table', 1),
	(7, '2018_04_18_164918_create_departments_table', 1),
	(8, '2018_04_19_043221_create_profiles_table', 1),
	(9, '2018_04_19_075736_create_department_user_table', 1),
	(10, '2018_04_19_102753_create_sap_user_table', 1),
	(11, '2018_04_20_011007_create_tickets_table', 1),
	(12, '2018_04_20_030137_modify_tickets_table', 1),
	(13, '2018_04_20_070421_create_t_attachments_table', 1),
	(14, '2018_04_23_104631_create_applications_table', 1),
	(15, '2018_04_23_233818_add_integration_application_id', 1),
	(16, '2018_04_24_043407_add_recurring_ticket_id', 1),
	(17, '2018_04_24_073447_add_ticket_number_to_tickets', 1),
	(18, '2018_04_24_145713_add_draft_column_to_tickets', 1),
	(19, '2018_04_24_223231_create_replies_table', 1),
	(20, '2018_04_24_224540_add_ticket_id_to_replies', 1),
	(21, '2018_04_25_093548_create_ticket_user_table', 1),
	(22, '2018_04_25_232910_add_hod_id_to_dept', 1),
	(23, '2018_05_03_225918_add_dates_to_tickets', 1),
	(24, '2018_05_04_002620_add_submitted_dates_to_tickets', 1),
	(25, '2018_05_04_112822_adding_readby_dates_to_tickets', 1),
	(26, '2018_05_28_131713_create_notifications_table', 1),
	(27, '2018_05_30_072444_add_assigned_date_to_tickets', 1),
	(28, '2014_10_12_000000_create_users_table', 1),
	(29, '2014_10_12_100000_create_password_resets_table', 1),
	(30, '2018_04_12_142435_prepare_users_table_for_social_authentication', 1),
	(31, '2018_04_12_142527_create_linked_social_accounts_table', 1),
	(32, '2018_04_16_130049_create_permission_tables', 1),
	(33, '2018_04_17_083355_create_saps_table', 1),
	(34, '2018_04_18_164918_create_departments_table', 1),
	(35, '2018_04_19_043221_create_profiles_table', 1),
	(36, '2018_04_19_075736_create_department_user_table', 1),
	(37, '2018_04_19_102753_create_sap_user_table', 1),
	(38, '2018_04_20_011007_create_tickets_table', 1),
	(39, '2018_04_20_030137_modify_tickets_table', 1),
	(40, '2018_04_20_070421_create_t_attachments_table', 1),
	(41, '2018_04_23_104631_create_applications_table', 1),
	(42, '2018_04_23_233818_add_integration_application_id', 1),
	(43, '2018_04_24_043407_add_recurring_ticket_id', 1),
	(44, '2018_04_24_073447_add_ticket_number_to_tickets', 1),
	(45, '2018_04_24_145713_add_draft_column_to_tickets', 1),
	(46, '2018_04_24_223231_create_replies_table', 1),
	(47, '2018_04_24_224540_add_ticket_id_to_replies', 1),
	(48, '2018_04_25_093548_create_ticket_user_table', 1),
	(49, '2018_04_25_232910_add_hod_id_to_dept', 1),
	(50, '2018_05_03_225918_add_dates_to_tickets', 1),
	(51, '2018_05_04_002620_add_submitted_dates_to_tickets', 1),
	(52, '2018_05_04_112822_adding_readby_dates_to_tickets', 1),
	(53, '2018_05_28_131713_create_notifications_table', 1),
	(54, '2018_05_30_072444_add_assigned_date_to_tickets', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table db_ticket.model_has_permissions
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.model_has_permissions: ~0 rows (approximately)
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;

-- Dumping structure for table db_ticket.model_has_roles
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.model_has_roles: ~9 rows (approximately)
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
	(1, 'App\\User', 1),
	(2, 'App\\User', 2),
	(1, 'App\\User', 3),
	(3, 'App\\User', 4),
	(2, 'App\\User', 5),
	(4, 'App\\User', 6),
	(3, 'App\\User', 7),
	(2, 'App\\User', 8),
	(5, 'App\\User', 9);
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;

-- Dumping structure for table db_ticket.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.notifications: ~4 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
	('02fe0a34-04e9-492e-9bb6-549cf15339b7', 'App\\Notifications\\TicketSubmitted', 'App\\User', 6, '{"message":"You have received a ticket - UM2018-BS-001","ticket_id":1}', '2018-09-14 00:36:46', '2018-09-05 15:46:27', '2018-09-14 00:36:46'),
	('14f633bc-b510-4f7c-aa5b-b4c9786aaa2c', 'App\\Notifications\\TicketApproved', 'App\\User', 2, '{"message":"Your ticket has been approved - UM2018-BS-001","ticket_id":1}', '2018-09-14 00:22:54', '2018-09-05 15:46:24', '2018-09-14 00:22:54'),
	('40f078fa-a8b2-4e27-bdc7-3f5e853b362d', 'App\\Notifications\\TicketApproved', 'App\\User', 2, '{"message":"Your ticket has been approved - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:37:57', '2018-09-14 00:37:57'),
	('8570e295-c4d0-42d3-9ea3-e484da7a4aa6', 'App\\Notifications\\TicketRejected', 'App\\User', 2, '{"message":"Your ticket has been rejected - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:33:11', '2018-09-14 00:33:11'),
	('8bd131f1-9c93-4960-ac88-a58290a329ab', 'App\\Notifications\\TicketSubmitted', 'App\\User', 6, '{"message":"You have received a ticket - UM2018-BS-002","ticket_id":2}', '2018-09-14 00:36:52', '2018-09-14 00:34:36', '2018-09-14 00:36:52'),
	('94358aed-b3f3-45ff-b187-53dc9d543a49', 'App\\Notifications\\TicketSubmitted', 'App\\User', 9, '{"message":"You have received a ticket - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:37:20', '2018-09-14 00:37:20'),
	('ad413376-9ccc-46f8-a8c3-01d2209bedba', 'App\\Notifications\\TicketSubmitted', 'App\\User', 9, '{"message":"You have received a ticket - UM2018-BS-001","ticket_id":1}', '2018-09-05 16:08:47', '2018-09-05 15:51:12', '2018-09-05 16:08:47'),
	('b14c931d-846d-424d-a1b3-0c81bcd6ba12', 'App\\Notifications\\TicketSubmitted', 'App\\User', 4, '{"message":"You have received a ticket - UM2018-BS-001","ticket_id":1}', '2018-09-05 15:33:22', '2018-09-05 15:32:13', '2018-09-05 15:33:22'),
	('ca776f4d-964a-4474-a52c-1803ec6e6d70', 'App\\Notifications\\TicketApproved', 'App\\User', 2, '{"message":"Your ticket has been approved - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:37:16', '2018-09-14 00:37:16'),
	('d2e5deca-64f4-4372-93b1-4b4340cb2d81', 'App\\Notifications\\TicketSubmitted', 'App\\User', 4, '{"message":"You have received a ticket - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:24:41', '2018-09-14 00:24:41'),
	('d78ed045-e6a8-4acc-80c8-ecb4201399a7', 'App\\Notifications\\TicketSubmitted', 'App\\User', 4, '{"message":"You have received a ticket - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:34:09', '2018-09-14 00:34:09'),
	('e34d9e72-24cc-4b94-8d18-2289b6115e2e', 'App\\Notifications\\TicketApproved', 'App\\User', 2, '{"message":"Your ticket has been approved - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:32:42', '2018-09-14 00:32:42'),
	('ee6a94b1-ca83-4929-b1ac-6dfe220ca36b', 'App\\Notifications\\TicketApproved', 'App\\User', 2, '{"message":"Your ticket has been approved - UM2018-BS-002","ticket_id":2}', NULL, '2018-09-14 00:34:33', '2018-09-14 00:34:33'),
	('f18a897c-0532-4baf-8cd0-86316d16859c', 'App\\Notifications\\TicketApproved', 'App\\User', 2, '{"message":"Your ticket has been approved - UM2018-BS-001","ticket_id":1}', '2018-09-14 00:22:51', '2018-09-05 15:51:08', '2018-09-14 00:22:51'),
	('f49b8ad8-be3c-457e-bbc9-9000772c621c', 'App\\Notifications\\TicketSubmitted', 'App\\User', 6, '{"message":"You have received a ticket - UM2018-BS-002","ticket_id":2}', '2018-09-14 00:36:55', '2018-09-14 00:32:47', '2018-09-14 00:36:55');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping structure for table db_ticket.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.password_resets: ~0 rows (approximately)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table db_ticket.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.permissions: ~26 rows (approximately)
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'view_users', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(2, 'add_users', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(3, 'edit_users', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(4, 'delete_users', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(5, 'view_roles', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(6, 'add_roles', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(7, 'edit_roles', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(8, 'delete_roles', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(9, 'view_saps', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(10, 'add_saps', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(11, 'edit_saps', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(12, 'delete_saps', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(13, 'view_departments', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(14, 'add_departments', 'web', '2018-09-05 11:26:56', '2018-09-05 11:26:56'),
	(15, 'edit_departments', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(16, 'delete_departments', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(17, 'view_tickets', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(18, 'add_tickets', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(19, 'edit_tickets', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(20, 'delete_tickets', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(21, 'view_applications', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(22, 'add_applications', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(23, 'edit_applications', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(24, 'delete_applications', 'web', '2018-09-05 11:26:57', '2018-09-05 11:26:57'),
	(26, 'view_profiles', 'web', '2018-09-05 11:49:38', '2018-09-05 11:49:38'),
	(27, 'add_profiles', 'web', '2018-09-05 11:49:38', '2018-09-05 11:49:38'),
	(28, 'edit_profiles', 'web', '2018-09-05 11:49:38', '2018-09-05 11:49:38'),
	(29, 'delete_profiles', 'web', '2018-09-05 11:49:38', '2018-09-05 11:49:38');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- Dumping structure for table db_ticket.profiles
CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hod_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_foreign` (`user_id`),
  KEY `profiles_hod_id_foreign` (`hod_id`),
  CONSTRAINT `profiles_hod_id_foreign` FOREIGN KEY (`hod_id`) REFERENCES `departments` (`id`),
  CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.profiles: ~9 rows (approximately)
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` (`id`, `user_id`, `avatar`, `created_at`, `updated_at`, `hod_id`) VALUES
	(1, 1, 'uploads/avatars/avatar1.jpg', '2018-09-05 11:27:05', '2018-09-05 11:27:05', NULL),
	(2, 2, 'uploads/avatars/avatar2.jpg', '2018-09-05 11:27:05', '2018-09-05 11:27:05', NULL),
	(3, 3, 'uploads/avatars/default.svg', '2018-05-31 17:47:29', '2018-05-31 17:47:29', NULL),
	(4, 4, 'uploads/avatars/default.svg', '2018-06-01 11:31:31', '2018-09-05 15:32:02', 4),
	(5, 5, 'uploads/avatars/default.svg', '2018-06-01 11:32:52', '2018-06-01 11:32:52', NULL),
	(6, 6, 'uploads/avatars/default.svg', '2018-06-01 11:39:46', '2018-09-05 15:38:07', 4),
	(7, 7, 'uploads/avatars/default.svg', '2018-06-01 11:44:06', '2018-06-01 11:44:06', NULL),
	(8, 8, 'uploads/avatars/default.svg', '2018-06-01 11:44:44', '2018-06-01 11:44:44', NULL),
	(9, 9, 'uploads/avatars/default.svg', '2018-06-01 11:46:59', '2018-09-05 15:46:04', 4);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;

-- Dumping structure for table db_ticket.replies
CREATE TABLE IF NOT EXISTS `replies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `body` text COLLATE utf8mb4_unicode_ci,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `replies_user_id_foreign` (`user_id`),
  KEY `replies_ticket_id_foreign` (`ticket_id`),
  CONSTRAINT `replies_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE,
  CONSTRAINT `replies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.replies: ~2 rows (approximately)
/*!40000 ALTER TABLE `replies` DISABLE KEYS */;
INSERT INTO `replies` (`id`, `body`, `user_id`, `created_at`, `updated_at`, `ticket_id`) VALUES
	(1, 'Noted, will check with Dasar.', 4, '2018-09-05 15:46:24', '2018-09-05 15:46:24', 1),
	(2, 'will forward this to PTM', 6, '2018-09-05 15:51:08', '2018-09-05 15:51:08', 1),
	(3, 'I will check on this...', 4, '2018-09-14 00:32:42', '2018-09-14 00:32:42', 2),
	(4, 'OK', 4, '2018-09-14 00:34:32', '2018-09-14 00:34:32', 2),
	(5, 'OK', 6, '2018-09-14 00:37:16', '2018-09-14 00:37:16', 2),
	(6, 'ALright..', 9, '2018-09-14 00:37:57', '2018-09-14 00:37:57', 2);
/*!40000 ALTER TABLE `replies` ENABLE KEYS */;

-- Dumping structure for table db_ticket.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.roles: ~6 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
	(1, 'Admin', 'web', '2018-09-05 11:27:03', '2018-09-05 11:27:03'),
	(2, 'User', 'web', '2018-09-05 11:27:04', '2018-09-05 11:27:04'),
	(3, 'HOD', 'web', '2018-05-30 18:13:12', '2018-05-30 18:13:12'),
	(4, 'Dasar', 'web', '2018-05-30 18:13:18', '2018-05-30 18:13:18'),
	(5, 'PTM', 'web', '2018-05-30 18:13:22', '2018-05-30 18:13:22'),
	(6, 'Brillante', 'web', '2018-05-30 18:13:27', '2018-05-30 18:13:27');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table db_ticket.role_has_permissions
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.role_has_permissions: ~74 rows (approximately)
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
	(1, 1),
	(2, 1),
	(3, 1),
	(4, 1),
	(5, 1),
	(6, 1),
	(7, 1),
	(8, 1),
	(9, 1),
	(10, 1),
	(11, 1),
	(12, 1),
	(13, 1),
	(14, 1),
	(15, 1),
	(16, 1),
	(17, 1),
	(18, 1),
	(19, 1),
	(20, 1),
	(21, 1),
	(22, 1),
	(23, 1),
	(24, 1),
	(26, 1),
	(27, 1),
	(28, 1),
	(29, 1),
	(1, 2),
	(5, 2),
	(7, 2),
	(9, 2),
	(13, 2),
	(17, 2),
	(18, 2),
	(19, 2),
	(20, 2),
	(21, 2),
	(26, 2),
	(28, 2),
	(3, 3),
	(7, 3),
	(11, 3),
	(15, 3),
	(17, 3),
	(19, 3),
	(23, 3),
	(27, 3),
	(3, 4),
	(7, 4),
	(11, 4),
	(15, 4),
	(17, 4),
	(19, 4),
	(23, 4),
	(27, 4),
	(3, 5),
	(7, 5),
	(11, 5),
	(15, 5),
	(17, 5),
	(19, 5),
	(23, 5),
	(26, 5),
	(28, 5),
	(3, 6),
	(7, 6),
	(11, 6),
	(15, 6),
	(17, 6),
	(19, 6),
	(23, 6),
	(26, 6),
	(28, 6);
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;

-- Dumping structure for table db_ticket.saps
CREATE TABLE IF NOT EXISTS `saps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.saps: ~9 rows (approximately)
/*!40000 ALTER TABLE `saps` DISABLE KEYS */;
INSERT INTO `saps` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
	(1, 'General Ledger', 'GL', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(2, 'Asset Accounting', 'AA', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(3, 'Treasury And Risk Management', 'TR', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(4, 'Accounts Payable', 'AP', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(5, 'Human Capital Management', 'HR', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(6, 'Funds Management', 'FM', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(7, 'Material Management', 'MM', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(8, 'Gst-related', 'GS', '2018-05-30 18:15:37', '2018-05-30 18:15:37'),
	(9, 'Basis', 'BS', '2018-05-30 18:15:37', '2018-05-30 18:15:37');
/*!40000 ALTER TABLE `saps` ENABLE KEYS */;

-- Dumping structure for table db_ticket.sap_user
CREATE TABLE IF NOT EXISTS `sap_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sap_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sap_user_sap_id_foreign` (`sap_id`),
  KEY `sap_user_user_id_foreign` (`user_id`),
  CONSTRAINT `sap_user_sap_id_foreign` FOREIGN KEY (`sap_id`) REFERENCES `saps` (`id`) ON DELETE CASCADE,
  CONSTRAINT `sap_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.sap_user: ~11 rows (approximately)
/*!40000 ALTER TABLE `sap_user` DISABLE KEYS */;
INSERT INTO `sap_user` (`id`, `sap_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 4, NULL, NULL),
	(2, 2, 4, NULL, NULL),
	(3, 1, 5, NULL, NULL),
	(4, 2, 6, NULL, NULL),
	(5, 4, 7, NULL, NULL),
	(6, 4, 8, NULL, NULL),
	(7, 5, 7, NULL, NULL),
	(8, 5, 9, NULL, NULL),
	(9, 2, 2, NULL, NULL),
	(10, 9, 2, NULL, NULL),
	(11, 8, 2, NULL, NULL),
	(12, 9, 4, NULL, NULL);
/*!40000 ALTER TABLE `sap_user` ENABLE KEYS */;

-- Dumping structure for table db_ticket.tickets
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sap_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ticket_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `dept_id` int(10) unsigned DEFAULT NULL,
  `integration` int(11) DEFAULT NULL,
  `application_id` int(10) unsigned DEFAULT NULL,
  `recurring_ticket_id` int(11) DEFAULT NULL,
  `ticket_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `approved_hod_date` datetime DEFAULT NULL,
  `rejected_hod_date` datetime DEFAULT NULL,
  `approved_dasar_date` datetime DEFAULT NULL,
  `rejected_dasar_date` datetime DEFAULT NULL,
  `approved_ptm_date` datetime DEFAULT NULL,
  `rejected_ptm_date` datetime DEFAULT NULL,
  `submitted_hod_date` datetime DEFAULT NULL,
  `submitted_dasar_date` datetime DEFAULT NULL,
  `submitted_ptm_date` datetime DEFAULT NULL,
  `readby_hod_date` datetime DEFAULT NULL,
  `readby_dasar_date` datetime DEFAULT NULL,
  `readby_ptm_date` datetime DEFAULT NULL,
  `assigned_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tickets_ticket_number_unique` (`ticket_number`),
  KEY `tickets_sap_id_foreign` (`sap_id`),
  KEY `tickets_user_id_foreign` (`user_id`),
  KEY `tickets_dept_id_foreign` (`dept_id`),
  KEY `tickets_application_id_foreign` (`application_id`),
  CONSTRAINT `tickets_application_id_foreign` FOREIGN KEY (`application_id`) REFERENCES `applications` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tickets_dept_id_foreign` FOREIGN KEY (`dept_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tickets_sap_id_foreign` FOREIGN KEY (`sap_id`) REFERENCES `saps` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tickets_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.tickets: ~1 rows (approximately)
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` (`id`, `subject`, `body`, `sap_id`, `user_id`, `ticket_type`, `created_at`, `updated_at`, `dept_id`, `integration`, `application_id`, `recurring_ticket_id`, `ticket_number`, `status`, `approved_hod_date`, `rejected_hod_date`, `approved_dasar_date`, `rejected_dasar_date`, `approved_ptm_date`, `rejected_ptm_date`, `submitted_hod_date`, `submitted_dasar_date`, `submitted_ptm_date`, `readby_hod_date`, `readby_dasar_date`, `readby_ptm_date`, `assigned_date`) VALUES
	(1, 'First Ticket', 'there is an issue with Basis. Can\'t sync user data with their profile records.', 9, 2, 'new', '2018-09-05 14:59:54', '2018-09-05 16:08:52', 4, 1, 1, NULL, 'UM2018-BS-001', 13, '2018-09-05 15:46:24', NULL, '2018-09-05 15:51:08', NULL, NULL, NULL, '2018-09-05 15:32:13', '2018-09-05 15:46:24', '2018-09-05 15:51:08', '2018-09-05 15:33:22', '2018-09-05 15:50:48', '2018-09-05 16:08:52', NULL),
	(2, 'Issues with BASIS', 'can\'t access the data..', 9, 2, 'new', '2018-09-14 00:24:00', '2018-09-14 00:37:57', 4, 1, 4, NULL, 'UM2018-BS-002', 9, '2018-09-14 00:34:33', '2018-09-14 00:33:11', '2018-09-14 00:37:16', NULL, '2018-09-14 00:37:57', NULL, '2018-09-14 00:34:09', '2018-09-14 00:34:33', '2018-09-14 00:37:16', '2018-09-14 00:34:22', '2018-09-14 00:36:28', '2018-09-14 00:37:48', NULL);
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;

-- Dumping structure for table db_ticket.ticket_user
CREATE TABLE IF NOT EXISTS `ticket_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ticket_user_user_id_foreign` (`user_id`),
  KEY `ticket_user_ticket_id_foreign` (`ticket_id`),
  CONSTRAINT `ticket_user_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ticket_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.ticket_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `ticket_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `ticket_user` ENABLE KEYS */;

-- Dumping structure for table db_ticket.t_attachments
CREATE TABLE IF NOT EXISTS `t_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ticket_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_attachments_ticket_id_foreign` (`ticket_id`),
  CONSTRAINT `t_attachments_ticket_id_foreign` FOREIGN KEY (`ticket_id`) REFERENCES `tickets` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.t_attachments: ~0 rows (approximately)
/*!40000 ALTER TABLE `t_attachments` DISABLE KEYS */;
INSERT INTO `t_attachments` (`id`, `path`, `ticket_id`, `created_at`, `updated_at`) VALUES
	(1, 'uploads/attachments/UM2018-BS-001-1536130794logo.png', 1, '2018-09-05 14:59:54', '2018-09-05 14:59:54'),
	(2, 'uploads/attachments/UM2018-BS-002-1536855840writing-notes-idea-conference.jpg', 2, '2018-09-14 00:24:00', '2018-09-14 00:24:00');
/*!40000 ALTER TABLE `t_attachments` ENABLE KEYS */;

-- Dumping structure for table db_ticket.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table db_ticket.users: ~9 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Giovanni Reynolds', 'uschneider@example.org', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'BY7ETyfp5TyK81qvtz0Dl31ybiLOcxJNkd4Cy7xvx643u8bP0UKMyGcliVYb', '2018-09-05 11:27:04', '2018-09-05 11:27:04'),
	(2, 'Paolo Hermiston', 'schoen.bertha@example.net', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'eZBAWNmuHJeoELAa57qedLhkkz97OsvzlqurX2ELv2DaA0e2dIzqlzkGOLYZ', '2018-09-05 11:27:05', '2018-09-05 11:27:05'),
	(3, 'Rizal Tamin', 'rizal@brillanteinsights.com', '$2y$10$A/zrlKedvyfdgksldKPBKO6e131V6Yz3O.IPrlODNvk1R7V2rFJDy', NULL, '2018-05-31 17:47:28', '2018-05-31 17:47:28'),
	(4, 'Asmila Harnete Mohamad', 'asmila@um.edu.my', '$2y$10$vUjYpEtpJWDuPGjoRFgog.QpmiM4ED6zEMjAKtHcM//JzwrPFbdfC', NULL, '2018-06-01 11:31:30', '2018-06-01 11:31:30'),
	(5, 'Shaleni S. Mutiah', 'shal@um.edu.my', '$2y$10$q.pCvFxRVt.2jdwi3orEc.2s2lYylWj8U8udh10GmCXKUpGeiiDKO', NULL, '2018-06-01 11:32:52', '2018-06-01 11:32:52'),
	(6, 'Siti Masyella Mokhtarrudin', 'syella@um.edu.my', '$2y$10$5UXB0a5WoD58No0c6BmVaeuCTLxlNkxCFUqImWugQ/Hy4OnVjB.cC', NULL, '2018-06-01 11:39:45', '2018-06-01 11:39:45'),
	(7, 'Dalila Md Idris', 'dalila@um.edu.my', '$2y$10$cotbN3ZP5SZZebIuBH0AVezsma5f4vhqzAxMyoeJde8q.qo4YlOy.', NULL, '2018-06-01 11:44:06', '2018-06-01 11:44:06'),
	(8, 'Normazila Jamal', 'normazila@um.edu.my', '$2y$10$eiU9QtdeAnYWkqT8X.MY5eTsH5MdbUqzvhWcCdkbmokDXZt7/IMS6', NULL, '2018-06-01 11:44:44', '2018-06-01 11:44:44'),
	(9, 'Nurhafidzah Mohd Zaini', 'fidzahzaini@um.edu.my', '$2y$10$fdfmVdzCQ9386PCEaKO3suFuyte8TDxgpS6QhiaC34pPMkMAmUbiq', '7WoCsRXwRrDEZDcglADW4GGH4PtitPiPNGfMvpX5JsHKjPVlYuakS4vCVq4E', '2018-06-01 11:46:59', '2018-06-01 11:46:59');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
